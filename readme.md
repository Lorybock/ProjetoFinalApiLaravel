<h1>laravel_api</h1>

Uma RESTFul API simples feito com Laravel, criada por Lorena Paiva de Figueiredo e Jessica Luana Oliveira, projeto elaborado para obtenção da nota final da disciplina Programação Web. 

Clone ou baixe este repositório e, estando no diretório do projeto duplique o arquivo '.env.example' e troque o nome do novo arquivo para '.env'. Abra o arquivo .env criado e troque DB_DATABASE=homestead para DB_DATABASE=api_laravel e DB_USERNAME e DB_PASSWORD para o username e o password da configuração do banco de dados de sua maquina, no seu banco de dados crie o BD api_laravel, e abra o terminal no projeto e inicie o servidor executando o comando "php artisan serve"


Para testar as requisições, utilize o software de sua preferência (Postman, por exemplo) e acesse: http://127.0.0.1:8000/api/courses/

Exemplo de requisição (GET):

URL: http://127.0.0.1:8000/api/courses/

A resposta será um json contendo os cursos cadastrados

------X--------

Exemplo de requisição (POST):

URL: http://127.0.0.1:8000/api/courses/

Body (JSON):

{
	"name": "Titulo do curso 1,
	"code": "INFO001",
    "description": "Primeira descrição",
	"price": "200",
    "credit_hour": "100",
	"instructor": "Fulano de Tal"
}

------X--------

Exemplo de requisição (PUT): (Trocar Content-Type de application/form-data para application/x-www-form-urlencoded a marcação do body)

URL: http://127.0.0.1:8000/api/courses/{id_do_curso}
Exemplo: http://127.0.0.1:8000/api/courses/1

Body (JSON):

{
	"name": "Titulo do curso 1 Alterado,
	"code": "INFO002",
    "description": "Descrição alterada",
	"price": "200",
    "credit_hour": "100",
	"instructor": "Fulano de Tal"
}

------X--------

Exemplo de requisição (PATCH):(Trocar Content-Type de application/form-data para application/x-www-form-urlencoded a marcação do body)

URL: http://127.0.0.1:8000/api/courses/{id_do_curso}
Exemplo: http://127.0.0.1:8000/api/courses/1

Body (JSON):

{
    "description": "Descrição alterada de novo",
}

------X--------

Exemplo de requisição (DELETE):

URL: http://127.0.0.1:8000/api/courses/{id_do_curso}
Exemplo: http://127.0.0.1:8000/api/courses/1

Body (JSON):

{
    
}

------X--------

Exemplo de requisição (OPTIONS):

URL: http://127.0.0.1:8000/api/courses/
Exemplo: http://127.0.0.1:8000/api/courses/

Body (JSON):

{
    
}