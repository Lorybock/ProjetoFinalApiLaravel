<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
    protected $fillable = ['code', 'name', 'description', 'credit_hour', 'instructor', 'price'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'courses';
}
