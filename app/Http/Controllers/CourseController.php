<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use Validator;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $courses = Course::all();
        $total = Course::all()->count();
        return compact('courses', 'total');
        // return Course::all();
        // return $courses;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // $course = new Course;
        // $course->id = $request->id;
        // $course->code = $request->code;
        // $course->name = $request->name;
        // $course->description = $request->description;
        // $course->credit_hour = $request->credit_hour;
        // $course->instructor = $request->instructor;
        // $course->price = $request->price;

    
        //   return new CourseResource($course);

          $data = $request->all();

          $validator = Validator::make($data, [
              'code' => 'required|string',
              'name' => 'required|string',
              'description' => 'required|string',
              'credit_hour' => 'required|integer|min:0',
              'instructor' => 'required|string',
              'price' => 'required|numeric|min:0'
          ]);

          if($validator->fails()) {
            return response()->json([
                'message'   => 'Validation Failed',
                'errors'    => $validator->errors()->all()
            ], 422);
        }


        $course = new Course();
        $course->fill($data);
        $course->save();

        return response()->json([
            'message'   => 'Course registered successfully!',
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    // public function updateRequest(Request $request) {
    //     $paramValue = $request->get('param_name');
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $course = Course::find($id);
        $data = $request->all();

        if(!$course) {
            return response()->json([
                'message'   => 'Record not found',
            ], 404);
        }

        if(!$data) {
            return response()->json([
                'message'   => 'Data not sent',
            ], 404);
        }

        $validator = Validator::make($data, [
            'code' => 'required|string',
            'name' => 'required|string',
            'description' => 'required|string',
            'credit_hour' => 'required|integer|min:0',
            'instructor' => 'required|string',
            'price' => 'required|numeric|min:0'
        ]);



        if($validator->fails()) {
            return response()->json([
                'message'   => 'Validation Failed',
                'errors'    => $validator->errors()->all()
            ], 422);
        }

        // $course = Course::findOrFail($id);
        $course->code = $request->code;
        $course->name = $request->name;
        $course->description = $request->description;
        $course->credit_hour = $request->credit_hour;
        $course->instructor = $request->instructor;
        $course->price = $request->price;
        $course->save();

        return response()->json([
            'message'   => 'Course updated successfully!',
        ], 200);
    }

    public function updateDescription(Request $request, $id)
    {
        //
        
        $course = Course::find($id);
        $data = $request->all();

        if(!$course) {
            return response()->json([
                'message'   => 'Record not found',
            ], 404);
        }

        if(!$data) {
            return response()->json([
                'message'   => 'Data not sent',
            ], 404);
        }

        $validator = Validator::make($data, [
            'description' => 'required|string',
        ]);



        if($validator->fails()) {
            return response()->json([
                'message'   => 'Validation Failed',
                'errors'    => $validator->errors()->all()
            ], 422);
        }


        $course->description = $request->description;
        $course->save();
        return response()->json([
            'message'   => 'The course description has been updated successfully!',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $course = Course::find($id);

        if(!$course) {
            return response()->json([
                'message'   => 'Record not found',
            ], 404);
        }
        $course->delete();
        return response()->json([
            'message'   => 'The course '.$course->name.' was deleted successfully!',
        ], 200);
    }

    public function option()
    {

        return response()->json([
            'Methods'   => 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
            'Content-Type' => 'application/json',
            'Required fields'=> 'To create and update(POST and PUT): code, name(Title), description, credit_hour, instructor and price',
            'Required fields - update description'=> 'To update description(PATCH): description'
        ], 200);
    }


}
